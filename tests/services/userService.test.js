import {User as UserDto} from "../../dto/User.js";
import User from "../../models/user.js";
import Follow from "../../models/follow.js";
import Tweet from "../../models/tweet.js";
import {UserService} from "../../services/userService.js";

describe('testing userService', () => {
    let userService;
    const USER1 = { name: 'Christian', nickname: 'christian'};
    const USER2 = { name: 'Diego', nickname: 'diego' };

    beforeEach(async() => {
        userService = new UserService();

        const usersToDestroy = await User.findAll();
        usersToDestroy.forEach((u) => {
            u.destroy();
        });

        const followsToDestroy = await Follow.findAll();
        followsToDestroy.forEach((f) => {
            f.destroy();
        });

        const tweetsToDestroy = await Tweet.findAll();
        tweetsToDestroy.forEach((t) => {
            t.destroy();
        });

        await userService.registerUser(USER1.name, USER1.nickname);
    });

   test('should create an instance of a new user with name and nickname', () => {
       let USER = new UserDto(USER1.name, USER1.nickname);

      expect(USER.name).toBe(USER1.name);
      expect(USER.nickname).toBe(USER1.nickname);
   });

   test('should register a new user with name and nickname', async() => {
       const users = await userService.getUsers();
       expect(users.length).toBe(1);
   });

   test('should register two new users', async() => {
       await userService.registerUser(USER2.name, USER2.nickname);

       const users = await userService.getUsers();
       expect(users.length).toBe(2);
   });

   test('If another person has been already registered using the same nickname an error is expected', async() => {
       await expect(userService.registerUser(USER2.name, USER1.nickname))
           .rejects
           .toThrow(Error);
   });

   test('The user can update his real name', async() => {
       const NEWNAME = 'Pedro';

       await userService.updateName(USER1.nickname, NEWNAME);

       const updatedUser = await userService.getUserByNickname(USER1.nickname);
       expect(updatedUser.name).toBe(NEWNAME);
   });

   test('should follow a user', async() => {
       await userService.registerUser(USER2.name, USER2.nickname);

       await userService.followUser(USER1.nickname, USER2.nickname);

       const followers = await userService.getFollowers(USER2.nickname);
       const totalFollowers = followers.length;
       expect(totalFollowers).toBe(1);
       expect(followers.includes(USER1.nickname)).toBe(true);
   });

   test('should get the correct followers between users', async() => {
       await userService.registerUser(USER2.name, USER2.nickname);

       await userService.followUser(USER1.nickname, USER2.nickname);
       await userService.followUser(USER2.nickname, USER1.nickname);

       const followersUser1 = await userService.getFollowers(USER1.nickname);
       const followersUser2 = await userService.getFollowers(USER2.nickname);
       const totalFollowersUser1 = followersUser1.length;
       const totalFollowersUser2 = followersUser2.length;

       expect(totalFollowersUser1).toBe(1);
       expect(totalFollowersUser2).toBe(1);

       expect(followersUser1.includes(USER2.nickname)).toBe(true);
       expect(followersUser2.includes(USER1.nickname)).toBe(true);
   });

    test('should make a tweet', async() => {
        const tweetMessage = 'test tweet';
        await userService.makeTweet(USER1.nickname, tweetMessage);
        const tweets = await userService.getTweets(USER1.nickname);
        const totalTweets = tweets.length;
        expect(totalTweets).toBe(1);
        expect(tweets).toEqual([tweetMessage]);
    });

   test('should make multiple tweets', async() => {
       const firstTweetMessage = 'first tweet';
       const secondTweetMessage = 'second tweet';
       await userService.makeTweet(USER1.nickname, firstTweetMessage);
       await userService.makeTweet(USER1.nickname, secondTweetMessage);
       const tweets = await userService.getTweets(USER1.nickname);
       const totalTweets = tweets.length;
       expect(totalTweets).toBe(2);
       expect(tweets).toEqual([firstTweetMessage, secondTweetMessage]);
   });
});