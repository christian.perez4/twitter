import {User as UserDto} from "../dto/User.js";
import {RepositoryCreator} from "../repository/repositoryCreator.js";

export class UserService {
    constructor() {
        const repositoryCreator = new RepositoryCreator();
        this.userRepository = repositoryCreator.create();
    }

    async registerUser(name, nickname) {
        let user = new UserDto(name, nickname);

        const users = await this.getUsers();
        if (users.find(u => u.nickname === nickname)) {
            throw new Error('user already exists');
        }

        await this.userRepository.registerUser(user);
    }

    async getUsers() {
        return await this.userRepository.getUsers();
    }

    async getUserByNickname(nickname) {
        const users = await this.userRepository.getUsers();
        return users.find(u => u.nickname === nickname);
    }

    async updateName(nickname, newName) {
        await this.userRepository.updateName(nickname, newName);
    }

    async followUser(nicknameFollower, nicknameToFollow) {
        await this.userRepository.followUser(nicknameFollower, nicknameToFollow);
    }

    async getFollowers(nickname) {
        const followers = await this.userRepository.getFollowers(nickname);
        return followers;
    }

    async makeTweet(nickname, tweetMessage) {
        await this.userRepository.makeTweet(nickname, tweetMessage);
    }

    async getTweets(nickname) {
        return await this.userRepository.getTweets(nickname);
    }
}