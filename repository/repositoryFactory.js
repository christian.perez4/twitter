import {RepositoryDb} from "./RepositoryDb.js";
import {RepositoryMemory} from "./RepositoryMemory.js";

export class RepositoryFactory {

    MEMORY = 1;
    DB = 2;

    createRepository(type) {
        switch (type) {
            case this.MEMORY:
                return new RepositoryMemory();
            case this.DB:
                return new RepositoryDb();
            default:
                return null;
        }
    }
}