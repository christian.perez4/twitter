import {RepositoryFactory} from "./repositoryFactory.js";

export class RepositoryCreator {

    create() {
        const repositoryFactory = new RepositoryFactory();
        return repositoryFactory.createRepository(repositoryFactory.DB);
    }
}