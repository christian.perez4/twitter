export class Repository {

    async registerUser(user) {
        throw new Error('Not implemented');
    }

    async getUsers() {
        throw new Error('Not implemented');
    }

    async updateName(nickname, newName) {
        throw new Error('Not implemented');
    }

    async followUser(nicknameFollower, nicknameToFollow) {
        throw new Error('Not implemented');
    }

    async getFollowers(nickname) {
        throw new Error('Not implemented');
    }

    async makeTweet(nickname, tweetMessage) {
        throw new Error('Not implemented');
    }

    async getTweets(nickname) {
        throw new Error('Not implemented');
    }
}