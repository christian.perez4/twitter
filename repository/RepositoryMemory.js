import {Repository} from "./repository.js";

export class RepositoryMemory extends Repository {

    constructor() {
        super();
        this.users = [];
        this.follows = [];
        this.tweets = [];
    }

    async registerUser(user) {
        this.users.push(user);
    }

    async getUsers() {
        return this.users;
    }

    async updateName(nickname, newName) {
        let userToUpdate = this.users.find(u => u.nickname === nickname);
        userToUpdate.name = newName;
    }

    async followUser(nicknameFollower, nicknameToFollow) {
        const follower = {
            user: {
                nickname: nicknameToFollow,
                followers: [nicknameFollower]
            }
        };

        this.follows.push(follower);
    }

    async getFollowers(nickname) {
        if (this.follows.length > 0) {
            return this.follows.find(f => f.user.nickname === nickname).user.followers;
        } else {
            return [];
        }
    }

    async makeTweet(nickname, tweetMessage) {
        const currentTweets = await this.getTweets(nickname);
        currentTweets.push(tweetMessage);
        const tweet = {
            user: {
                nickname: nickname,
                tweets: currentTweets
            }
        };

        console.log(tweet, 'lksjdlkfasjkdaf')

        this.tweets.push(tweet);
    }

    async getTweets(nickname) {
        if (this.tweets.length > 0) {
            return this.tweets.find(f => f.user.nickname === nickname).user.tweets;
        } else {
            return [];
        }
    }
}