import {Repository} from "./repository.js";
import User from "../models/user.js";
import Follow from "../models/follow.js";
import Tweet from "../models/tweet.js";
import {User as UserDto} from "../dto/User.js";

export class RepositoryDb extends Repository {

    constructor() {
        super();
    }

    async registerUser(user) {
        try {
            const newUser = {
                name: user.name,
                nickname: user.nickname
            }

            await User.create(newUser);
        } catch(error) {
            throw new Error('Exception error');
        }
    }

    async getUsers() {
        try {
            const users = await User.findAll();

            let outputUsers = [];
            users.forEach(e => {
                let user = new UserDto(e.dataValues.name, e.dataValues.nickname);
                outputUsers.push(user);
            });
            return outputUsers;
        } catch(error) {
            throw new Error(error);
        }
    }

    async updateName(nickname, newName) {
        try {
            const userFound = await User.findOne({
                where: { nickname: nickname }
            });
            const updateUser = {
                name: newName,
            }
            await userFound.update(updateUser);
        } catch(error) {
            throw new Error(error);
        }
    }

    async followUser(nicknameFollower, nicknameToFollow) {
        try {
            const newFollower = {
                nickname: nicknameToFollow,
                followerNickname: nicknameFollower
            }

            await Follow.create(newFollower);
        } catch(error) {
            throw new Error(error);
        }
    }

    async getFollowers(nickname) {
        try {
            const followers = await Follow.findAll({
                where: { nickname: nickname }
            });

            let outputFollowers = [];
            followers.forEach(f => {
                outputFollowers.push(f.dataValues.followerNickname);
            });

            return outputFollowers;
        } catch(error) {
            throw new Error(error);
        }
    }

    async makeTweet(nickname, tweetMessage) {
        try {
            const newTweet = {
                nickname: nickname,
                message: tweetMessage
            }

            await Tweet.create(newTweet);
        } catch(error) {
            throw new Error('Exception error');
        }
    }

    async getTweets(nickname) {
        try {
            const tweets = await Tweet.findAll({
                where: { nickname: nickname }
            });

            let outputTweets = [];
            tweets.forEach(f => {
                outputTweets.push(f.dataValues.message);
            });

            return outputTweets;
        } catch(error) {
            throw new Error(error);
        }
    }
}